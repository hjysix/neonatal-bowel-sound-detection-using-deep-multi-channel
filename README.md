# README #


### Codes and results for the under-reviewed paper: Neonatal Bowel Sound Detection Using Deep Multi-channel Convolution Neural Network and Laplace Hidden Semi-Markov Model###

Codes for implementing the proposed model and reproducing the experimental results reported in this paper can be found in the jupyter notebooks in the 'code' folder under this repository. 
Pre-trained models are also provided to allow diret model applications on new unseen data.   
It is worth note that, for privacy protection, personal information and ausculatory recodings of the patients involved in our study have been removed. 